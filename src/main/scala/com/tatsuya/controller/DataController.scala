package com.tatsuya.controller

import com.tatsuya.view.View
import org.apache.spark.sql.{DataFrame, SparkSession}

class DataController(spark: SparkSession) {

  import spark.implicits._

  private var path: String = ""
  private var view: View = new View()

  def processData(): Unit = {
    var data: DataFrame = spark.read.parquet(path)
  }
}
