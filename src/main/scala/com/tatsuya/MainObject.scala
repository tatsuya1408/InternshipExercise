package com.tatsuya

import com.tatsuya.controller.DataController
import org.apache.spark.sql.SparkSession

object MainObject {
  def main(args: Array[String]): Unit = {
    var spark: SparkSession = SparkSession.builder()
      .master("local")
      .appName("Internship Exercise")
      .getOrCreate()
    var controller: DataController = new DataController(spark)
    controller.processData()
  }
}
